#!flask/bin/python
import json
import string
import random
from flask import Flask
from flask import request
from flask import jsonify
random.seed(50)

app = Flask(__name__)


def create_board(N=5):
    # Create board using random alphbets A-Z(Both upper and lower case)
    # and whitespace
    board = [['A' for i in range(N)] for i in range(N)]
    for i in range(0, N):
        for j in range(0, N):
            board[i][j] = random.choice(string.ascii_letters + " ")

    return board


@app.route('/run-board', methods=["POST"])
def board_demo():

    # Set random seed for reproducibility
    random.seed(50)
    # Get json data
    json_data = request.json
    # Create 2D array of alphabest
    board = create_board()

    # List of words to find
    x_array = [
        'rxO',
        'SEXvf',
        'SIf',
        ' g',
        'xowo',
        'e'
    ]

    # Create transpose of the board
    t_board = map(list, [*zip(*board)])
    # Join the letters of main and transposed board
    lst = [("").join(t_word) for t_word in t_board] + [
        ("").join(word) for word in board]

    # Iterate over x_array and all the board words to find matching
    matching_words = set()
    for x_word in x_array:
        for word in lst:
            if x_word in word:
                matching_words.add(x_word)

    # Return response
    return jsonify({
        "matching_words": list(matching_words),
        "board": board,
        "words_to_find": x_array
    }), 200

if __name__ == '__main__':
    app.run(debug=True)
